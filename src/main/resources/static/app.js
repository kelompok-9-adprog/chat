var stompClient = null;

function setConnected(connected) {
    document.getElementById('connect').disabled = connected;
    document.getElementById('disconnect').disabled = !connected;
    document.getElementById('conversationDiv').style.visibility
        = connected ? 'visible' : 'hidden';
    document.getElementById('response').innerHTML = '';
}

function connect() {
    var socket = new SockJS('/chat');
    var fromId = document.getElementById('fromId').value;
    var toId = document.getElementById('toId').value;
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/' + fromId + '/' + toId
            , function(messageOutput) {
            showMessageOutput(JSON.parse(messageOutput.body));
        });
        const Http = new XMLHttpRequest();
        Http.open("GET", '/get-chat-between/?from=' + fromId + "&to=" + toId, true);
        Http.send();
        Http.onreadystatechange = function() {
            if (Http.readyState == 4 && Http.status == 200) {
                const data = JSON.parse(Http.responseText);
                for (let i = 0; i < data.length; i++) {
                    showMessageOutput(data[i]);
                }
            }
        };
    });
    /**
    setTimeout(function() {
        const Http = new XMLHttpRequest();
        Http.open("GET", '/get-chat-between/?from=' + fromId + "&to=" + toId, true);
        Http.send();
        Http.onreadystatechange = function() {
            if (Http.readyState == 4 && Http.status == 200) {
                const data = JSON.parse(Http.responseText);
                for (let i = 0; i < data.length; i++) {
                    showMessageOutput(data[i]);
                }
            }
        };
        //your code to be executed after 1 second
    }, 1000);
    */
}

function disconnect() {
    if(stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendMessage() {
    var fromId = document.getElementById('fromId').value;
    var toId = document.getElementById('toId').value;
    var text = document.getElementById('text').value;
    stompClient.send("/app/chat", {},
        JSON.stringify({'fromId': fromId, 'toId': toId, 'text':text}));
}

function showMessageOutput(messageOutput) {
    var response = document.getElementById('response');
    var p = document.createElement('p');
    p.style.wordWrap = 'break-word';
    p.appendChild(document.createTextNode(messageOutput.fromUserId + ": "
        + messageOutput.chat + " (" + messageOutput.timestamp + ")"));
    console.log(p);
    response.appendChild(p);
}