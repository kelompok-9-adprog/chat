package admodb.chat;

public class Message {

    private int fromId;
    private int toId;
    private String text;

    // getters and setters

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int from) {
        this.fromId = from;
    }

    public int getToId() {
        return toId;
    }

    public void setToId(int toId) {
        this.toId = toId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}