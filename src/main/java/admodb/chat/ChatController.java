package admodb.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

@Controller
public class ChatController {
    @Autowired
    private ChatObjectRepository chatObjectRepository;

    @Autowired
    private SimpMessagingTemplate msgTmp;

    @PostMapping("/create-chat")
    public ResponseEntity<ChatObject> createChat(
            @RequestParam(value="from", required=true) int from,
            @RequestParam(value="to", required=true) int to,
            @RequestParam(value="chat", required=true) String chat) {
        ChatObject chatObject = new ChatObject();
        chatObject.setFromUserId(from);
        chatObject.setToUserId(to);
        System.out.println(from + " " + to);
        chatObject.setChat(chat);
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        chatObject.setTimestamp(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date()));
        chatObjectRepository.save(chatObject);
        return ResponseEntity.ok().body(chatObject);
    }

    @ResponseBody
    @GetMapping(value="/get-chat/")
    public Iterable<ChatObject> getAllChat() {
        return chatObjectRepository.findAll();
    }

    @ResponseBody
    @GetMapping(value="/get-chat-between/")
    public Iterable<ChatObject> getChatWith(
            @RequestParam(value="from", required=true) int from,
            @RequestParam(value="to", required=true) int to) {
        LinkedList<ChatObject> allChat = new LinkedList<ChatObject>();
        Iterable<ChatObject> iter = chatObjectRepository.findAll();
        Iterator<ChatObject> iterator = iter.iterator();
        while (iterator.hasNext()) {
             ChatObject chat = iterator.next();
             if (chat.getFromUserId() == from && chat.getToUserId() == to || chat.getFromUserId() == to && chat.getToUserId() == from) {
                allChat.add(chat);
             }
        }
        return allChat;
    }

    @MessageMapping("/chat")
    public void send(Message message) throws Exception {
        String time = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date());

        ChatObject chatObject = new ChatObject();
        chatObject.setFromUserId(message.getFromId());
        chatObject.setToUserId(message.getToId());
        chatObject.setChat(message.getText());
        chatObject.setTimestamp(time);
        chatObjectRepository.save(chatObject);
        msgTmp.convertAndSend("/topic/" + message.getFromId() + "/" + message.getToId(), chatObject);

        msgTmp.convertAndSend("/topic/" + message.getToId() + "/" + message.getFromId(), chatObject);
    }
}