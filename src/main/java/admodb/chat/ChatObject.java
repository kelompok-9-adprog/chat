package admodb.chat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ChatObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    private String chat;

    private int fromUserId;

    private int toUserId;

    private String timestamp;

    public String getChat() {
        return chat;
    }

    public void setChat(String chat) {
        this.chat = chat;
    }

    public int getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(int from) {
        this.fromUserId = from;
    }

    public int getToUserId() {
        return toUserId;
    }

    public void setToUserId(int to) {
        this.toUserId = to;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
