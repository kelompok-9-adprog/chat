package admodb.chat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChatObjectTests {

    @Test
    public void createEntityTest() {
        ChatObject chatObject = new ChatObject();
        chatObject.setFromUserId(1);
        chatObject.setToUserId(2);
        chatObject.setChat("Test");

        Assert.assertEquals(chatObject.getFromUserId(), 1);
        Assert.assertEquals(chatObject.getToUserId(), 2);
        Assert.assertEquals(chatObject.getChat(), "Test");
    }
}
