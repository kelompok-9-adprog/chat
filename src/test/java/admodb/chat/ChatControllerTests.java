package admodb.chat;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChatControllerTests {

    @Autowired
    ChatController controller = new ChatController();

    @Test
    public void createChatObjectTest() {
        int from = 1;
        int to = 2;
        String chat = "Test";

        controller.createChat(from, to, chat);
        Iterable<ChatObject> iter = controller.getAllChat();
        Assert.assertTrue(true);
    }
}
